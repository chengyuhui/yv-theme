</section>
<?php get_sidebar() ?>
</div>
<footer role="contentinfo">
	<p>Content © Yonder Voice. Proudly powered by <a href="http://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform." rel="nofollow">WordPress</a>.</p>
	<p>Original "Floral Belle" theme by <a href="http://www.nickifaulk.com/">Nicki Faulk</a>, CSS3 rewrite by <a href="http://github.com/chengyuhui">Harry Cheng</a>.</p>
	<p>Header image by <a href="http://www.pixiv.net/member.php?id=546819">Vima</a>.
		Background Image by <a href="http://squidfingers.com/patterns/">Squid Fingers</a>. Icon by <a title="Freepik" href="http://www.freepik.com" rel="nofollow">Freepik</a> from <a title="Flaticon" href="http://www.flaticon.com" rel="nofollow">Flaticon</a></p>
</footer>
</main>
<?php wp_footer(); ?>
<!-- <?php echo get_num_queries(); ?> queries -->
<script>
	(function() {
		var _gaq = window._gaq || [];
		_gaq.push([
			'_trackTiming',
			'Backend',
			'Render Time',
			<?php timer_stop( 1 )?>,
			'Backend(PHP) Time',
			80
		]);
	})();
</script>
</body>
</html>
