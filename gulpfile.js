var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var filter = require('gulp-filter');
var debug = require('gulp-debug');

var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var mqpacker = require('css-mqpacker');
var oldie = require('oldie');
var doiuse = require('doiuse');
var assets  = require('postcss-assets');
var cssnano = require('cssnano');

var rename = require('gulp-rename');
var svgSprite = require('gulp-svg-sprite');
var del = require('del');
var imagemin = require('gulp-imagemin');

var join = require('path').join;


var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

const STYLES = ['sass/**/*.scss'];
const STYLE_DEST = 'css';

const ICON = ['img/icon/*.svg'];
const ICON_DEST = 'img';

const COPY_FILES = [
    'includes/**/*',
    '**/*.php',
    '!build/**/*',
    'acf-json/*.json',

    'img/symbol/svg/*.svg',

    'css/**/*',
    'fonts/**/*',
    'style.css'
];
const COPY_DEST = 'build';

const IMG = [
    'img/*',
    '!*.psd'
];
const IMG_DEST = join(COPY_DEST, 'img');

const AUTOPREFIXER_BROWSERS = [
    'last 2 version',
    '> 10% in JP',
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 5',
    'bb >= 10'
];

var errHandler = {
    errorHandler: notify.onError({
        message: "Error: <%= error.message %>"
    })
};

var sassOptions = {};


gulp.task('build',['styles', 'svg', 'imagemin'], function () {
    gulp.src(COPY_FILES, {base: '.'})
        .pipe(gulp.dest(COPY_DEST));
});

gulp.task('clean', function () {
    return del(COPY_DEST);
});

gulp.task('imagemin', function () {
    return gulp.src(IMG)
        .pipe(imagemin())
        .pipe(gulp.dest(IMG_DEST))
});

gulp.task('browser-sync', function () {
    browserSync.init({
        proxy: 'localhost',
        open: true,
        injectChanges: true
    });
});

gulp.task('styles', function () {
    return gulp.src(STYLES)
        .pipe(plumber(errHandler))
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([
            assets({
                loadPaths: ['img/', 'fonts/'],
                relative: 'true',
                cachebuster: function () {
                    return process.env['REVISION'] || 'dev';
                }
            }),
            autoprefixer({
                browsers: AUTOPREFIXER_BROWSERS
            }),
            doiuse({
                browsers: AUTOPREFIXER_BROWSERS
            }),
            mqpacker()
        ]))
        .pipe(gulp.dest(STYLE_DEST))
        .pipe(filter(["**", "!css/editor.css"])) // Exclude editor.css
        .pipe(browserSync.stream())

        .pipe(rename({suffix: '.min'}))
        .pipe(postcss([cssnano({ safe: true })]))
        .pipe(gulp.dest(STYLE_DEST))

        .pipe(postcss([oldie()]))
        .pipe(rename({suffix: '.oldie'}))
        .pipe(gulp.dest(STYLE_DEST));
});

gulp.task('svg', function() {
    return gulp.src(ICON)
        .pipe(svgSprite({
            mode: {
                symbol: {
                    inline: true
                }
            }
        }))
        .pipe(gulp.dest(ICON_DEST));
});

gulp.task('default', ['browser-sync', 'styles', 'svg'], function () {
    gulp.watch(STYLES, ['styles']);
    gulp.watch(ICON, ['svg']);
    gulp.watch("*.php").on("change", reload);
});