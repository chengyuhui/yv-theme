<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php is_home() ? bloginfo('name') : wp_title('') ?></title>

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!-- WP Head -->
	<?php wp_head(); ?>
	<!-- /WP Head -->
	<!--[if lt IE 9]>
	<script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script>
	<![endif]-->
</head>
<body <?php body_class(); ?>>
<?php echo file_get_contents( __DIR__ . '/img/symbol/svg/sprite.symbol.svg' ); //SVG Sprite?>
<!--[if lt IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
	your browser</a> to improve your experience.</p>
<![endif]-->
<main>
	<header>
		<a class="logo" href="<?php echo home_url() ?>">
			<span><?php bloginfo( 'name' ) ?></span>
		</a>
		<span id="menu-toggle">
			<svg><use xlink:href="#menu"/></svg>
		</span>

		<?php wp_nav_menu( array(
			"theme_location" => 'header-menu',
			'container'      => 'nav',
			'container_id' => 'header-menu'
		) ); ?>
	</header>
	<div class="marquee">
		<span>Incoming Events</span>
		<?php while( have_rows('scroll_rep', 'options') ): the_row();?>
			<span><a href="<?php the_sub_field('url')?>">-<?php the_sub_field('text')?></a></span>
		<?php endwhile;?>
	</div>
	<div class="main-container">
	<section id="content">
