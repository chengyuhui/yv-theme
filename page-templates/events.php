<?php /* Template Name: 活动列表 */ ?>
<?php get_header() ?>
<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" class="post">
			<h3 class="item-head"><a href="<?php the_permalink() ?>"
			                         rel="bookmark"><?php the_title(); ?></a></h3>
			<div class="post-content">
				<?php the_content() ?>
			</div>
		</article>
	<?php endwhile; ?>
<?php endif;
get_footer();
