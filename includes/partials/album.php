<div class="album">
	<a href="<?php the_permalink() ?>" class="album-cover"><img src="<?php the_field( 'cover' ) ?>"></a>

	<div class="album-info">
		<h3 class="album-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a>
		</h3>
		<section class="album-description">
			<?php the_field( 'description' ) ?>
		</section>
	</div>
</div>