<?php
class YV_Social_Widget extends YV_Base_Widget {
	public function __construct() {
		parent::__construct('yv_social', 'Social Media Link');
	}

	public function widget_content( $args, $instance ) {
		$id = 'widget_'.$args['widget_id'];

		if (have_rows('links', $id)) :
			$title = apply_filters('widget_title', $instance['title']);
			echo $args['before_widget'];
			if(!empty($title)) {
				echo $args['before_title']. $title . $args['after_title'];
			}
			?>
			<ul>
				<?php while( have_rows('links', $id) ): the_row();
					$url = get_sub_field('url');
					$title = get_sub_field('site_name');
					$svg = get_sub_field('svg_icon_id');
					?>
					<li>
						<a href="<?php echo $url ?>"
						   title="<?php echo $title ?>">
							<svg class="site-icon"><use xlink:href="#<?php echo $svg ?>"/></svg>
						</a>
					</li>
				<?php endwhile;?>
			</ul>
			<?php
			echo $args['after_widget'];
		endif;
	}
}