<?php
class YV_RecentThumb_Widget extends YV_Base_Widget {
	public function __construct() {
		parent::__construct(
			'yv_recent_thumb', // Base ID
			'Recent Works Thumb'
		);
		add_action( 'save_post', array( $this, 'purge_cache' ) );
		add_action( 'deleted_post', array( $this, 'purge_cache' ) );
		add_action( 'switch_theme', array( $this, 'purge_cache' ) );
	}

	public function widget_content( $args, $instance ) {
		$query_args = array(
			'meta_key' => 'banner',
			'meta_value' => '',
			'meta_compare' => '!='
		);

		$posts = get_posts( $query_args );

		if (!$posts) return;

		$title = apply_filters('widget_title', $instance['title']);

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		echo "<ul>";

		global $post;
		foreach ($posts as $post):
			setup_postdata($post);
			if (false == ($banner = get_field( 'banner' ))) continue; // Filter again
			?>
			<li>
				<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
					<img src="<?php echo $banner?>" alt="<?php the_title_attribute()?>" class="banner-thumb">
				</a>
			</li>
		<?php endforeach;

		echo "</ul>";
		wp_reset_postdata();
		echo $args['after_widget'];
	}
}