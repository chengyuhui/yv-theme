<?php

/**
 * Class YV_Base_Widget
 */
abstract class YV_Base_Widget extends WP_Widget {
	public $cache_enabled = true;
	public function widget( $args, $instance ) {
		if (!$this->cache_enabled) {
			$this->widget_content( $args, $instance );
			return;
		}

		$tr = get_transient( $this->id );

		if ( ! empty( $tr ) ) {
			echo $tr;
			echo "<!-- Cache Hit: $this->id -->";

			return;
		}

		ob_start();
		$this->widget_content( $args, $instance );
		set_transient( $this->id, ob_get_flush(), 0 );
		echo "<!-- Cache Added: $this->id -->";
	}

	/**
	 * The actual widget frontend function, will be called by widget().
	 *
	 * @param $args
	 * @param $instance
	 *
	 * @throws Exception
	 */
	public abstract function widget_content( $args, $instance );

	public function purge_cache() {
		delete_transient( $this->id );
	}

	public function update( $new_instance, $old_instance ) {
		$this->purge_cache();

		return $new_instance;
	}

	/**
	 * A form with only title, actual form is added by ACF later, this is used to show "Save" button.
	 *
	 * @param array $instance
	 *
	 * @return null
	 */
	public function form( $instance ) {
		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = '';
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
			       name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
			       value="<?php echo esc_attr( $title ); ?>"/>
		</p>
		<?php
	}
}