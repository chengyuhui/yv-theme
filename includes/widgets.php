<?php
include(__DIR__.'/widgets/base.php');
include(__DIR__.'/widgets/social.php');
include(__DIR__.'/widgets/recent_thumb.php');

add_action('widgets_init', function() {
	register_widget( 'YV_Social_Widget' );
	register_widget( 'YV_RecentThumb_Widget' );
});